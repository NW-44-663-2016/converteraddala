﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ConverterAddala.Models;

namespace ConverterAddala.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Converter App by Addala";
            ViewData["Result"] = "";
            //ViewData["Image"] = @"wwwroot\Images\transparent.png";

            Converter converter = new Converter();
            return View(converter);
        }
        public IActionResult Convert(Converter converter)
        {
            ViewData["Title"] = "Converter App by Addala";
            if (ModelState.IsValid)
            {
                ViewData["Title"] = "Converted by Addala";
                ViewData["Result"] = "Temperature in C = " +
                    (int)((converter.Temperature_F - 32) * 5.0 / 9.0);
                //ViewData["Image"] = @"wwwroot\Images\cold.png";
            }
            return View("Index", converter);
        }
    }
}
